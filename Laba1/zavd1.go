// Laba1 project main.go
package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c int
	fmt.Println("Enter a , b, c: ")
	fmt.Scanf("%d", &a)
	fmt.Scanf("%d", &b)
	fmt.Scanf("%d", &c)
	fmt.Printf("%dx^2 + %dx + %d = 0\n", a, b, c)
	D := b*b - 4*a*c
	fmt.Printf("D = %d\n", D)
	if D < 0 {
		fmt.Println("Рівняння не має коренів")
	} else if D == 0 {
		x := (b / 2 * a) * -1
		fmt.Printf("Корінь рівняння один - %f", x)
	} else {
		sqrt := math.Sqrt(float64(D))
		x1 := ((float64(b) * -1) + sqrt) / 2 * float64(a)
		x2 := ((float64(b) * -1) - sqrt) / 2 * float64(a)
		fmt.Printf("x1 = %f\n", x1)
		fmt.Printf("x2 = %f\n", x2)
	}
}
