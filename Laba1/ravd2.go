package main

import (
	"fmt"
)

func main() {
	fmt.Println("ax + by = c")
	fmt.Println("dx + ey = f")
	fmt.Println("Enter a b c d e f :")
	var a, b, c, d, e, f float64
	fmt.Scanf("%f", &a)
	fmt.Scanf("%f", &b)
	fmt.Scanf("%f", &c)
	fmt.Scanf("%f", &d)
	fmt.Scanf("%f", &e)
	fmt.Scanf("%f", &f)
	x := (a*f - c*d) / (a*e - b*d)
	y := (c*e - b*f) / (a*e - b*d)
	fmt.Printf("X = %f\n", x)
	fmt.Printf("Y = %f\n", y)
}
