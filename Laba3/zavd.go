package main

import (
	"fmt"
	"math"
)

func main() {
	numberCount := 3000
	x := int64(1)
	var array [3000]int64
	for i := 0; i < numberCount; i++ {
		array[i] = Con(&x)
		//fmt.Println(array[i])
	}
	mMap := GetP(array)
	for k := range mMap {
		fmt.Println(k, " - ", mMap[k])
	}
	spod := MathSpod(mMap)
	fmt.Println("Математичне сподівання : ", spod)
	disp := Disp(mMap, spod)
	fmt.Println("Дисперсія : ", disp)
	fmt.Println("Середньоквадратичне відхилення : ", math.Sqrt(disp))
}

func Con(x0 *int64) int64 {
	m := int64(math.Pow(2, 32))
	a := int64(65539)
	c := int64(0)
	res := int64(0)
	*(x0) = ((a * *(x0)) + c) % m
	res = *(x0) % int64(200)
	return res
}

func IsInMap(mass map[int64]float64, key int64) bool {
	if mass[key] == 0 {
		return false
	}
	return true
}

func GetNumCountInArray(array [3000]int64, num int64) int64 {
	count := 0
	for i := 0; i < len(array); i++ {
		if array[i] == num {
			count++
		}
	}
	return int64(count)
}

func GetP(array [3000]int64) map[int64]float64 {
	myMap := make(map[int64]float64)
	for i := 0; i < len(array); i++ {
		if !IsInMap(myMap, array[i]) {
			myMap[array[i]] = float64(GetNumCountInArray(array, array[i])) / 3000
		}
	}
	return myMap
}

func MathSpod(myMap map[int64]float64) float64 {
	var spod float64 = 0
	for k := range myMap {
		spod = spod + myMap[k]*float64(k)
	}
	return spod
}
func MathSpodSquare(myMap map[int64]float64) float64 {
	var spod float64 = 0
	for k := range myMap {
		spod = spod + myMap[k]*float64(k)*float64(k)
	}
	return spod
}

func Disp(myMap map[int64]float64, mathSpod float64) float64 {
	var disp float64 = 0
	spodSquare := MathSpodSquare(myMap)
	disp = spodSquare - mathSpod*mathSpod
	return disp
}
