// main
package main

import "fmt"
import mymath "golab/lab2/ex3/math"

func main() {
	sum := mymath.Add(1.3, 2.33, -3.2)
	fmt.Println(sum)
}
