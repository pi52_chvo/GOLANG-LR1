package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	//Задание.
	//1. Создайте 2 переменные  разных типов. Выпоните арифметические операции. Результат вывести
	var a int8 = 12
	var b int16 = 20
	var sum = int(a) + int(b)
	fmt.Println("a + b = ", sum)
	var riz = int(a) - int(b)
	fmt.Println("a - b = ", riz)
	var mul = int(a) * int(b)
	fmt.Println("a * b = ", mul)
}
